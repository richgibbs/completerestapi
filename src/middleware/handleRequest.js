const handleRequest = handler => async (req, res) => {
  try {
    await handler(req, res);
  } catch (ex) {
    console.log(`ex = ${ex.message}`);
  }
};

module.exports = handleRequest;
