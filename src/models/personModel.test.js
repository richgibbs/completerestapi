const { validate } = require('./personModel');

const validFullPersonRequest = {
  firstname: 'Richard',
  surname: 'Gibbs',
  age: '33',
  sex: 'Male',
  parents: [
    {
      firstname: 'Dorothy',
      surname: 'Gibbs',
      sex: 'Female'
    },
    {
      firstname: 'Philip',
      surname: 'Gibbs',
      sex: 'Male'
    }
  ]
};

const validPartialPersonRequest = {
  firstname: 'Richard',
  surname: 'Gibbs',
  age: '33',
  sex: 'Male'
};

// Valid Requests
describe('Valid Person Request', () => {
  it('should pass a valid person request', () => {
    const { error } = validate(validFullPersonRequest);
    expect(error).toBeUndefined();
  });

  it('should pass a pertial person request', () => {
    const { error } = validate(validPartialPersonRequest);
    expect(error).toBeUndefined();
  });
});

// Invalid Requests
describe('Invalid Person Request', () => {
  it('should fail an invalid person request', () => {
    validPartialPersonRequest.sex = 'sss';
    const { error } = validate(validPartialPersonRequest);
    const formatError = error.details[0].message.replace(/"/g, '');
    expect(formatError).toBe('sex must be one of [Male, Female]');
    validPartialPersonRequest.sex = 'Male';
  });
});
