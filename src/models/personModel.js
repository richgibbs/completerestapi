const Joi = require('@hapi/joi');

const validSex = ['Male', 'Female'];

const schema = Joi.object({
  firstname: Joi.string().required(),
  surname: Joi.string().required(),
  age: Joi.number().required(),
  sex: Joi.string()
    .valid(...validSex)
    .required(),
  parents: Joi.array().items(
    Joi.object().keys({
      sex: Joi.string()
        .valid(...validSex)
        .required(),
      firstname: Joi.string().required(),
      surname: Joi.string().required()
    })
  )
});

const vlaidateRequest = input => schema.validate(input);

module.exports.validate = vlaidateRequest;
