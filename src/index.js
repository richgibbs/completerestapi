require('dotenv').config();
const fs = require('fs');
const https = require('https');
const config = require('config');
const express = require('express');
const app = require('./app');
const logger = require('./utils/logger');

const { PORT, SERVER_KEY_PATH, SERVER_CERT_PATH } = config.get('app');

if (!PORT) throw new Error('No port specified');
if (!SERVER_KEY_PATH) throw new Error('No SERVER_KEY_PATH specified');
if (!SERVER_CERT_PATH) throw new Error('No SERVER_CERT_PATH specified');

const srv = https
  .createServer(
    {
      key: fs.readFileSync(SERVER_KEY_PATH),
      // passphrase: process.env.SERVER_KEY_PASSPHRASE,
      cert: fs.readFileSync(SERVER_CERT_PATH),
      requestCert: false,
      rejectUnauthorised: false
    },
    app
  )
  .listen(PORT, () => logger.debug(`Listening on port ${PORT}`));

module.exports = {
  srv,
  PORT
};
