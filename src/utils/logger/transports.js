const { exceptions, format, transports, add, info } = require('winston');

const applicationName =
  process.env.APPLICATION_IDENTIFIER || 'Unknown Application';

const { combine, timestamp, simple, label, json, cli, errors } = format;
const applicationLogFile = `${applicationName}.log`;

const exceptionTransports = () => {
  exceptions.handle(
    new transports.Console({
      format: combine(
        cli(),
        errors(),
        label({ label: applicationName, message: true }),
        simple()
      )
    }),
    new transports.File({
      filename: applicationLogFile,
      format: combine(
        errors({ stack: true }),
        timestamp(),
        label({ label: applicationName, message: true }),
        json()
      )
    })
  );

  process.on('unhandledRejection', ex => {
    info(
      `Unhandled Promise rejection: see ${applicationLogFile}\n${ex.message}`
    );
  });

  process.on('uncaughtException', ex => {
    info(`Uncaught Exception: see ${applicationLogFile}\n${ex.message}`);
  });
};

const loggingHandler = () => {
  exceptionTransports();

  add(
    new transports.Console({
      level: process.env.LOG_LEVEL,
      format: combine(
        cli(),
        errors(),
        label({ label: applicationName, message: true }),
        simple()
      )
    })
  );

  add(
    new transports.File({
      level: process.env.LOG_LEVEL,
      filename: applicationLogFile,
      format: combine(
        cli(),
        errors({ stack: true }),
        timestamp(),
        label({ label: applicationName, message: true }),
        json()
      )
    })
  );
};

module.exports = loggingHandler;
