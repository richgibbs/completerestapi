require('dotenv').config();

const { log, info, error, warn, verbose, debug, silly } = require('winston');

const transports = require('./transports')();

const applicationName =
  process.env.APPLICATION_IDENTIFIER || 'Unknown Application';

const requestErrorResponse = detail => {
  const resp = {
    success: false,
    error: {
      errors: detail,
      app: applicationName
    }
  };

  error(JSON.stringify(resp));
  return resp;
};

module.exports = {
  requestErrorResponse,
  log,
  info,
  error,
  warn,
  verbose,
  debug,
  silly,
  transports
};
