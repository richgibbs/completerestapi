const express = require('express');
const config = require('config');
const routes = require('./routes');
const logger = require('./utils/logger');
const passportAuth = require('./passport');

const {
  BODY_PARSER_LIMIT,
  BODY_PARSER_PARAMETER_LIMIT,
  REQUEST_PATH_ROOT
} = config.get('app');

const { NODE_ENV } = process.env;

const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
    limit: BODY_PARSER_LIMIT,
    parameterLimit: BODY_PARSER_PARAMETER_LIMIT
  })
);

app.use((req, res, next) => {
  console.log('just received a request');
  req.config = {
    xxx: '123'
  };
  passportAuth.auth(req, res, next);
  next();
});

app.use(express.json({ limit: BODY_PARSER_LIMIT }));
app.use(REQUEST_PATH_ROOT, routes);

module.exports = app;
