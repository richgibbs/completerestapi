logger = require('./utils/logger');

let myPassport;

const auth = (req, res, next) => {
  myPassport = req.config.xxx;
  logger.info(`im in passport myPassport = ${myPassport}`);
  next();
};

logger.info(`myPassport globally in passport = ${myPassport}`);

module.exports = {
  auth
};
