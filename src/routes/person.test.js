const person = require('./person');

const myResponse = {
  message: 'person found'
};

const personRequest = {
  body: {
    firstname: 'Richard',
    surname: 'Gibbs',
    age: '33',
    sex: 'Male',
    parents: [
      {
        firstname: 'Dorothy',
        surname: 'Gibbs',
        sex: 'Female'
      },
      {
        firstname: 'Philip',
        surname: 'Gibbs',
        sex: 'Male'
      }
    ]
  }
};

const mockResponse = () => {
  const res = { response: myResponse };
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res.response);
  return res;
};

describe('Person search', () => {
  it('Happy path', async done => {
    const res = mockResponse();
    await person.searchPerson(personRequest, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith(myResponse);
    done();
  });

  it('Invalid path', async done => {
    const res = mockResponse();
    const req = {
      body: {}
    };
    await person.searchPerson(req, res);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: { errors: 'firstname is required', app: 'MyApp' }
    });
    done();
  });
});
