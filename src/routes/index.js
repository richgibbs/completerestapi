const express = require('express');

const router = express.Router();

router.use(require('./person'));

//catch all requests not handled by the above route
router.use((req, res) => {
  res.status(400).json({ message: 'route not found' });
});

module.exports = router;
