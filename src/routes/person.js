const express = require('express');
const handleRequest = require('../middleware/handleRequest');
const logger = require('../utils/logger');
const { validate } = require('../models/personModel');

const router = express.Router();

// ********************************************************************
// Main Handler
// ********************************************************************
const getPersonHandler = handleRequest((req, res) => {
  logger.info(`now inside person`);

  const { error } = validate(req.body);

  if (error) {
    const errorDetail = error.details[0].message.replace(/"/g, '');

    const errorResponse = logger.requestErrorResponse(errorDetail);
    res.status(500).json(errorResponse);
    return;
  }
  console.log('handling person route');
  res.status(200).json({ message: 'person found' });
});

router.get('/person', getPersonHandler);

module.exports = router;
module.exports.searchPerson = getPersonHandler;
