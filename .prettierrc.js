module.exports = {
  tabWidth: 2,
  semicolons: true,
  singleQuote: true,
  trailingComma: 'none'
};
