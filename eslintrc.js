module.exports = {
  extends: ['airbnb-base', 'plugin: prettier/recommended'],
  plugins: ['prettier', 'jest'],
  rules: {
    'prettier/prettier': ['error'],
    'no-underscore-dangle': 0
  },
  env: {
    es6: true,
    node: true,
    jest: true
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules', 'src']
      }
    }
  },
  parser: 'babel-eslint'
};
